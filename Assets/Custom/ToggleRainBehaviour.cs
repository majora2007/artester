﻿using UnityEngine;
using System.Collections;

public class ToggleRainBehaviour : MonoBehaviour,
									IVirtualButtonEventHandler {
	
	private ParticleSystem particleSystemComp = null;
	private bool particleSystemEnabled = true;
	

	// Use this for initialization
	void Start () {
		
		VirtualButtonBehaviour vb = GetComponent<VirtualButtonBehaviour>();
		if (vb)
		{
			vb.RegisterEventHandler(this);
		} else {
			Debug.LogError("Virtual Button Behaviour not found");
		}
		
		particleSystemComp = GetComponent<ParticleSystem>();
		
		if (particleSystemComp == null)
		{
			Debug.LogError("Partile System Comp is null!");
		}
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void IVirtualButtonEventHandler.OnButtonPressed(VirtualButtonBehaviour vb)	
	{
		
	}
	
	void IVirtualButtonEventHandler.OnButtonReleased(VirtualButtonBehaviour vb)
	{
		if (particleSystemEnabled)
		{
			particleSystemEnabled = false;
		} else {
			particleSystemEnabled = true;
		}
		
		particleSystemComp.particleEmitter.enabled = particleSystemEnabled;
	}
	
	
}
